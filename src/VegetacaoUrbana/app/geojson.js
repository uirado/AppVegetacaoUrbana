import React, { Component } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Registry from "./components/registry/registry.component";
import { colors } from "./app-styles";
import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: `https://api.graph.cool/simple/v1/cjgzoroyz04qe01463r8s7979`
});

const allRegistriesQuery = gql`
  query {
    allRegistries {
      id
      createdAt
      updatedAt
      annotations
      type
      height
      latitude
      longitude
      lotDistance
      streetDistance
      width
      length
      trunkDiameter
      cupDiameter
    }
  }
`;

export default function exportToFile(region) {
  client.query({ query: allRegistriesQuery }).then(result => {
    console.log(result);

    if (!result.loading && result.data && result.data.allRegistries) {
      data = result.data.allRegistries;

      let response = {
        type: "FeatureCollection",
        features: []
      };

      data.forEach(r => {
        //check region
        if (inRegion(region, r.latitude, r.longitude)) {
          props = {};
          props.color = resolveColor(r.type);
          if (r.createdAt) props.createdAt = r.createdAt;
          if (r.updatedAt) props.updatedAt = r.updatedAt;
          if (r.annotations) props.annotations = r.annotations;
          if (r.images) props.images = r.images;
          if (r.type) props.images = r.images;
          if (r.height) props.height = r.height;
          if (r.lotDistance) props.lotDistance = r.lotDistance;
          if (r.streetDistance) props.streetDistance = r.streetDistance;
          if (r.width) props.width = r.width;
          if (r.length) props.length = r.length;
          if (r.trunkDiameter) props.trunkDiameter = r.trunkDiameter;
          if (r.cupDiameter) props.cupDiameter = r.cupDiameter;

          response.features.push({
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [r.longitude, r.latitude]
            },
            properties: props
          });
        }
      });
      console.log(JSON.stringify(response));
    }
  });
}

function inRegion(region, lat, lng) {
  let { latitude, longitude, latitudeDelta, longitudeDelta } = region;
  if (
    latitude - latitudeDelta <= lat &&
    lat <= latitude + latitudeDelta &&
    longitude - longitudeDelta <= lng &&
    lng <= longitude + longitudeDelta
  )
    return true;
  return false;
}

function resolveColor(type) {
  if (type == "SMALL") {
    return colors.primary;
  }
  return colors.darkGreen;
}
