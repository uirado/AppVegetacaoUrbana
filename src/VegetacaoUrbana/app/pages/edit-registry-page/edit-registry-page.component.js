import React, { Component } from "react";
import {
  View,
  Text,
  Picker,
  TextInput,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  Button,
  Image,
  Dimensions
} from "react-native";
import { LatLng } from "react-native-maps";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Page from "../../pages/model-page/model-page.component";
import styles from "./edit-registry-page.styles";
import { colors } from "../../app-styles";
import { form } from "../../constants";
import ConfirmationOptions from "../../components/confirmation-options/confirmation-options";
import PropTypes from "prop-types";
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import LoadingIcon from "../../components/loading-icon.component";
import { AppState } from "../../state-manager";
import ImagePicker from "react-native-image-picker";

const options = {
  title: "Select Avatar",
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

const GET_REGISTRY_BY_ID = gql`
  query Registry($id: ID!) {
    Registry(id: $id) {
      id
      createdAt
      updatedAt
      annotations
      latitude
      longitude
      images
      type
      height
      lotDistance
      streetDistance
      width
      length
      trunkDiameter
      cupDiameter
    }
  }
`;

export default class EditRegistry extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Page backgroundColor={colors.lightGray}>
        <Query query={GET_REGISTRY_BY_ID} variables={{ id: this.props.id }}>
          {({ loading, error, data }) => {
            if (loading) return <LoadingIcon message="Carregando" />;
            if (error) {
              this.registry = {
                type: "SMALL",
                latitude: this.props.latitude,
                longitude: this.props.longitude
              };
            } else {
              if (data && data.Registry) {
                this.registry = data.Registry;
              } else {
                return <Text>Erro ao carregar dados!</Text>;
              }
            }
            console.log(this.registry);
            return (
              <RegistryContent
                data={this.registry}
                confirmAction={this.props.confirmAction}
                cancelAction={this.props.cancelAction}
              />
            );
          }}
        </Query>
      </Page>
    );
  }
}

class RegistryContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: props.data
    };
  }

  render() {
    let { width, height } = Dimensions.get("window");
    const { icon, size } = resolveType(this.state.data.type);
    return (
      <View style={{ paddingBottom: 51, flex: 1 }}>
        <ScrollView contentContainerStyle={styles.container}>
          <View
            style={[
              styles.row,
              { paddingHorizontal: 5, justifyContent: "space-between" }
            ]}
          >
            {this.state.data.createdAt && (
              <Text style={styles.dates}>
                Criado em: {this.state.data.createdAt.substr(0, 10)}
              </Text>
            )}

            {this.state.data.createdAt && (
              <Text style={styles.dates}>
                Modificado em: {this.state.data.createdAt.substr(0, 10)}
              </Text>
            )}
          </View>
          <Card>
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              {this.state.data.images && (
                <Image
                  style={{
                    width: width / 2,
                    height: width / 2,
                    resizeMode: Image.resizeMode.cover
                  }}
                  source={{
                    uri: this.state.data.images[0]
                  }}
                />
              )}
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: colors.secondary,
                    borderRadius: 20,
                    padding: 7,
                    elevation: 2,
                    margin: 5,
                    marginHorizontal: 10
                  }}
                  onPress={() => {
                    ImagePicker.launchCamera(options, response => {
                      console.log(response);
                      if (response.data) {
                        this.updateData("images", [
                          `data:${response.type};base64,${response.data}`
                        ]);
                      }
                    });
                  }}
                >
                  <Icon name="camera" size={20} color={colors.white} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor: colors.secondary,
                    borderRadius: 20,
                    padding: 7,
                    elevation: 2,
                    margin: 5,
                    marginHorizontal: 10
                  }}
                  onPress={() => {
                    ImagePicker.launchImageLibrary(options, response => {
                      if (response.data) {
                        this.updateData("images", [
                          `data:${response.type};base64,${response.data}`
                        ]);
                      }
                    });
                  }}
                >
                  <Icon name="image" size={20} color={colors.white} />
                </TouchableOpacity>
              </View>
            </View>
          </Card>
          <Card>
            <View style={styles.typeContainer}>
              <View style={{ width: 40, alignItems: "center" }}>
                <Icon
                  name={icon}
                  size={size}
                  color={
                    (this.state.data.type == "SMALL" && colors.primary) ||
                    colors.darkGreen
                  }
                />
              </View>
              <Picker
                selectedValue={this.state.data.type}
                onValueChange={value => {
                  this.updateData("type", value);
                }}
                style={{ flex: 1, height: form.inputHeight }}
              >
                <Picker.Item label="Pequeno porte" value="SMALL" />
                <Picker.Item label="Médio porte" value="MEDIUM" />
                <Picker.Item label="Grande porte" value="LARGE" />
                <Picker.Item label="Palmeira" value="PALM" />
                <Picker.Item label="Vegetação rasteira" value="UNDERBRUSH" />
              </Picker>
            </View>
          </Card>

          {this.state.data.type != "UNDERBRUSH" && (
            <Card title="Medidas:">
              <View style={styles.row}>
                <Text style={styles.text}>Altura: </Text>
                <TextInput
                  placeholderTextColor="#ccc"
                  onChangeText={value => {
                    this.updateData("height", Number(value));
                  }}
                  placeholder="Não informado"
                  keyboardType="numeric"
                  value={
                    this.state.data.height && String(this.state.data.height)
                  }
                  style={styles.inputText}
                />
                <Text style={styles.unit}>m</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.text}>Largura do tronco:</Text>
                <TextInput
                  placeholderTextColor="#ccc"
                  onChangeText={value => {
                    this.updateData("trunkDiameter", Number(value));
                  }}
                  placeholder="Não informado"
                  value={
                    this.state.data.trunkDiameter &&
                    String(this.state.data.trunkDiameter)
                  }
                  keyboardType="numeric"
                  style={styles.inputText}
                />
                <Text style={styles.unit}>cm</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.text}>Largura da copa:</Text>
                <TextInput
                  placeholderTextColor="#ccc"
                  onChangeText={value => {
                    this.updateData("cupDiameter", Number(value));
                  }}
                  placeholder="Não informado"
                  value={
                    this.state.data.cupDiameter &&
                    String(this.state.data.cupDiameter)
                  }
                  keyboardType="numeric"
                  style={styles.inputText}
                />
                <Text style={styles.unit}>m</Text>
              </View>
            </Card>
          )}
          {this.state.data.type == "UNDERBRUSH" && (
            <Card title="Medidas:">
              <View style={styles.row}>
                <Text style={styles.text}>Largura:</Text>
                <TextInput
                  placeholderTextColor="#ccc"
                  onChangeText={value => {
                    this.updateData("width", Number(value));
                  }}
                  placeholder="Não informado"
                  value={this.state.data.width && String(this.state.data.width)}
                  keyboardType="numeric"
                  style={styles.inputText}
                />
                <Text style={styles.unit}>m</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.text}>Comprimento:</Text>
                <TextInput
                  placeholderTextColor="#ccc"
                  onChangeText={value => {
                    this.updateData("length", Number(value));
                  }}
                  placeholder="Não informado"
                  value={
                    this.state.data.length && String(this.state.data.length)
                  }
                  keyboardType="numeric"
                  style={styles.inputText}
                />
                <Text style={styles.unit}>m</Text>
              </View>
            </Card>
          )}

          <Card title="Distâncias:">
            <View style={styles.row}>
              <Text style={styles.text}>Do tronco ao lote: </Text>
              <TextInput
                placeholderTextColor="#ccc"
                onChangeText={value => {
                  this.updateData("lotDistance", Number(value));
                }}
                placeholder="Não informado"
                value={
                  this.state.data.lotDistance &&
                  String(this.state.data.lotDistance)
                }
                keyboardType="numeric"
                keyboardType="numeric"
                style={styles.inputText}
              />
              <Text style={styles.unit}>m</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>Do tronco à rua: </Text>
              <TextInput
                placeholderTextColor="#ccc"
                onChangeText={value => {
                  this.updateData("streetDistance", Number(value));
                }}
                placeholder="Não informado"
                value={
                  this.state.data.streetDistance &&
                  String(this.state.data.streetDistance)
                }
                keyboardType="numeric"
                style={styles.inputText}
              />
              <Text style={styles.unit}>m</Text>
            </View>
          </Card>
          <Card title="Anotações">
            <TextInput
              placeholderTextColor="#ccc"
              onChangeText={value => {
                this.updateData("annotations", value);
              }}
              value={
                this.state.data.annotations &&
                String(this.state.data.annotations)
              }
              placeholder="Escreva uma anotação..."
              multiline={true}
              style={{
                alignContent: "flex-start"
              }}
            />
          </Card>
          {this.state.data.id && (
            <Mutation
              mutation={DELETE_REGISTRY}
              onCompleted={() => {
                this.props.cancelAction();
              }}
            >
              {(deleteRegistry, { loading, returnData }) => {
                if (loading) return <LoadingIcon simple message="Apagando" />;

                return (
                  <View style={{ padding: 5 }}>
                    <Button
                      onPress={() => {
                        deleteRegistry({
                          variables: { id: this.state.data.id }
                        });
                      }}
                      title="Apagar"
                      color={colors.danger}
                    />
                  </View>
                );
              }}
            </Mutation>
          )}
        </ScrollView>
        <Mutation
          mutation={(this.props.data.id && UPDATE_REGISTRY) || CREATE_REGISTRY}
          onCompleted={() => {
            this.props.cancelAction();
          }}
        >
          {(createRegistry, { loading, returnData }) => {
            if (loading) return <LoadingIcon simple message="Salvando" />;

            return (
              <ConfirmationOptions
                cancelAction={this.props.cancelAction}
                confirmAction={() => {
                  createRegistry({ variables: this.state.data });
                }}
                confirmTitle="Salvar"
              />
            );
          }}
        </Mutation>
      </View>
    );
  }

  updateData(chave, valor) {
    let obj = {};
    obj[chave] = valor;
    let newData = Object.assign({}, this.state.data, obj);
    this.setState({
      data: newData
    });
  }
}

const UPDATE_REGISTRY = gql`
  mutation updateRegistry(
    $id: ID!
    $type: String!
    $latitude: Float!
    $longitude: Float!
    $annotations: String
    $images: [String!]
    $height: Float
    $trunkDiameter: Float
    $cupDiameter: Float
    $lotDistance: Float
    $streetDistance: Float
  ) {
    updateRegistry(
      id: $id
      latitude: $latitude
      longitude: $longitude
      type: $type
      images: $images
      annotations: $annotations
      height: $height
      trunkDiameter: $trunkDiameter
      cupDiameter: $cupDiameter
      lotDistance: $lotDistance
      streetDistance: $streetDistance
    ) {
      id
    }
  }
`;

const CREATE_REGISTRY = gql`
  mutation createRegistry(
    $type: String!
    $latitude: Float!
    $longitude: Float!
    $annotations: String
    $images: [String!]
    $height: Float
    $trunkDiameter: Float
    $cupDiameter: Float
    $lotDistance: Float
    $streetDistance: Float
  ) {
    createRegistry(
      latitude: $latitude
      longitude: $longitude
      type: $type
      images: $images
      annotations: $annotations
      height: $height
      trunkDiameter: $trunkDiameter
      cupDiameter: $cupDiameter
      lotDistance: $lotDistance
      streetDistance: $streetDistance
    ) {
      id
    }
  }
`;
const DELETE_REGISTRY = gql`
  mutation deleteRegistry($id: ID!) {
    deleteRegistry(id: $id) {
      id
    }
  }
`;

class Card extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    title = String(this.props.title).toUpperCase();
    return (
      <View style={styles.card}>
        {this.props.title && <Text style={styles.cardTitle}>{title}</Text>}
        {this.props.children}
      </View>
    );
  }
}

EditRegistry.propTypes = {
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired
};

EditRegistry.defaultProps = {};

function resolveType(type) {
  response = {
    icon: "circle",
    size: 15
  };
  switch (type) {
    case "MEDIUM":
      response.size = 25;
      break;
    case "LARGE":
      response.size = 35;
      break;
    case "PALM":
      response.size = 20;
      response.icon = "asterisk";
      break;
    case "UNDERBRUSH":
      response.size = 20;
      response.icon = "barley";
  }
  return response;
}
