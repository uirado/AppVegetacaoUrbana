import { StyleSheet } from "react-native";
import { colors } from "../../app-styles";
import { form } from "../../constants";

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  card: {
    margin: 5,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: colors.white,
    elevation: 2,
    borderRadius: 3
  },
  dates: {
    fontSize: 10
  },
  cardTitle: {
    alignSelf: "center",
    fontSize: 12,
    fontWeight: "bold"
  },
  typeContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  text: {
    width: "50%",
    textAlign: "right"
  },
  inputText: {
    textAlign: "right",
    width: 100,
    height: form.inputHeight
  },
  unit: {
    width: 30
  }
});

export default styles;
