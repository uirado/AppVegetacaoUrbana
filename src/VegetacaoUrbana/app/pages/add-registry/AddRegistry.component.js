import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import ConfirmationOptions from "../../components/confirmation-options/confirmation-options";
import { headerHeight } from "../../app-styles";
import Pino from "../../components/pino.component";

export default class AddRegistry extends Component {
  render() {
    return (
      <View style={styles.container} pointerEvents="box-none">
        <View style={{ alignItems: "center" }}>
          <Pino />
        </View>
        <ConfirmationOptions
          cancelAction={this.props.cancelAction}
          confirmAction={this.props.confirmAction}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: headerHeight,
    flex: 1,
    width: "100%",
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "center"
  }
});
