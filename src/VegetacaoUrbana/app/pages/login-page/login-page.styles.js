import { StyleSheet } from "react-native";
import { colors } from "../../app-styles";

const styleLoginPage = StyleSheet.create({
  styleLoginView: {
    flex: 1,
    width: "100%",
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "flex-end",
    backgroundColor: colors.primary
  },
  generalForm: {
    padding: "10%"
  },
  styleLoginButton: {
    alignItems: "center",
    padding: 5,
    backgroundColor: "white"
  },
  styleSignInButton: {
    alignItems: "center"
  },
  styleSignInText: {
    alignSelf: "center",
    padding: 5,
    color: colors.white
  },
  styleIconAndName: {
    padding: "10%",
    alignItems: "center"
  },
  styleNameApp: {
    fontSize: 30,
    fontWeight: "bold"
  }
});

export default styleLoginPage;
