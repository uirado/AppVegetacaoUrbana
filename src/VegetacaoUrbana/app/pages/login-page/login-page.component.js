import React, { Component } from "react";
import { TextInput, TouchableHighlight, View, Text } from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styleLoginPage from "./login-page.styles";
import { colors } from "../../app-styles";

export default class LoginPage extends Component {
  render() {
    return (
      <View style={styleLoginPage.styleLoginView}>
        <View style={styleLoginPage.styleIconAndName}>
          <Icon name="tree" size={200} />
          <Text style={styleLoginPage.styleNameApp}>Vegetação Urbana</Text>
        </View>
        <View style={styleLoginPage.generalForm}>
          <TextInput placeholder="Email" />
          <TextInput placeholder="Senha" secureTextEntry={true} />
          <TouchableHighlight
            style={styleLoginPage.styleLoginButton}
            activeOpacity={50}
            onPress={() => this.props.navigation.navigate("Home")}
          >
            <Text>Entrar</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styleLoginPage.styleSignInButton}
            onPress={() => this.props.navigation.navigate("Signup")}
          >
            <Text style={styleLoginPage.styleSignInText}>Cadastrar</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}
