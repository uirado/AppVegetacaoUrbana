import React, { Component } from "react";
import { View, BackHandler, ToastAndroid, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import { BrazilRegion, HeaderTitles } from "../../constants";
import { AppState, getState, getInitialState } from "../../state-manager";
import Mapa from "../../components/map/map.component";
import Header from "../../components/header/header.component";
import Notification from "../../components/notification/notification.component";
import styles from "./home-page.styles";
import EditRegistry from "../edit-registry-page/edit-registry-page.component";
import TrunkPlant, {
  TrunkPlantType
} from "../../components/registry/trunk-plant.component";
import BigButton from "../../components/generic-button/generic-button.component";
import AddRegistry from "../add-registry/AddRegistry.component";
import Registries from "../../queries/registries.query";
import BottomStatusBar from "../../components/bottom-status-bar.component";
import LoginPage from "../login-page/login-page.component";
import exportToFile from "../../geojson";

export default class HomePage extends Component {
  constructor(props) {
    super(props);

    this.alreadyZoomed = false;
    this.stateStack = [];

    this.state = getInitialState();
    this.stateStack.push(this.state);

    //bind(this) é para permitir que um child utilize uma função do pai que tem referência ao pai
    this.onMapRender = this.onMapRender.bind(this);
    this.toggleNotification = this.toggleNotification.bind(this);
    this.newRegistry = this.newRegistry.bind(this);
    this.gotoState = this.gotoState.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <Mapa
          ref={component => (this.map = component)}
          onRender={this.onMapRender}
          region={this.state.region}
          moveOnMarkerPress={this.state.moveOnMarkerPress}
        >
          {this.state.zoom > 14 && (
            <Registries zoom={this.state.zoom} gotoState={this.gotoState} />
          )}
        </Mapa>

        {this.state.appState != AppState.login &&
          this.state.appState != AppState.signUp && (
            <Header
              title={this.state.headerTitle}
              subTitle={this.state.headerSubTitle}
              leftIcon={this.state.headerLeftIcon}
              rightIcon={
                (this.state.appState == AppState.home && "file-export") ||
                this.state.headerRightIcon
              }
              leftAction={null}
              rightAction={
                (this.state.appState == AppState.home &&
                  (() => exportToFile(this.state.region))) ||
                this.toggleNotification
              }
            />
          )}

        {this.state.appState == AppState.login && (
          <LoginPage
            gotoState={() => {
              this.gotoState(AppState.home);
            }}
          />
        )}

        {this.state.appState == AppState.home && (
          <BigButton
            onPress={() => this.gotoState(AppState.addRegistry)}
            icon="plus"
          />
        )}

        {this.state.appState == AppState.addRegistry && (
          <AddRegistry
            cancelAction={() => this.gotoState(AppState.home)}
            confirmAction={() => this.gotoState(AppState.editRegistry)}
          />
        )}

        {this.state.appState == AppState.editRegistry && (
          <EditRegistry
            cancelAction={() => this.gotoState(AppState.home)}
            confirmAction={this.newRegistry}
            latitude={this.state.region.latitude}
            longitude={this.state.region.longitude}
            id={this.state.registryId}
          />
        )}

        {this.state.appState != AppState.login &&
          this.state.appState != AppState.signUp && (
            <Notification
              show={this.state.showNotification}
              text={this.state.notificationText}
              type="information"
              closeAction={this.toggleNotification}
            />
          )}
      </View>
    );
  }

  newRegistry(newRegistry) {
    //adicionar registro no banco e depois redirecionar para home
    this.gotoState(AppState.home);
  }

  toggleNotification() {
    this.setState({ showNotification: !this.state.showNotification });
  }

  gotoState(nextAppState, params) {
    newState = getState(nextAppState, this.state);
    //se voltou pra home, reseta a pilha de estados
    if (nextAppState == AppState.home) {
      this.stateStack = [];
    }

    if (params) {
      newState = Object.assign({}, newState, params);
    }

    this.map.gotoRegion(newState.region);
    this.setState(newState, () => {
      this.stateStack.push(this.state);
    });
  }

  goBack() {
    if (this.stateStack.length > 1) {
      this.stateStack.pop();
      length = this.stateStack.length;
      lastState = this.stateStack[length - 1];

      this.map.gotoRegion(lastState.region);
      this.setState(lastState);
    }
  }

  onMapRender(newRegion) {
    if (this.alreadyZoomed == false) {
      this.map.zoomToUser();
      this.alreadyZoomed = true;
    }

    let shouldRender = false;
    let zoom = calcZoom(newRegion.longitudeDelta);
    if (zoom != this.state.zoom) {
      //caso tenha dado zoom
      shouldRender = true;
    }

    this.setState(
      {
        zoom: zoom,
        region: newRegion,
        shouldRender: shouldRender
      },
      () => {
        this.stateStack[this.stateStack.length - 1] = this.state;
      }
    );
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    //impedir renderização desnecessária
    if (nextState.shouldRender == false) {
      nextState.shouldRender = true;
      return false;
    }
    return true;
  }

  componentDidMount() {
    //configura o botão voltar aparelho
    BackHandler.addEventListener("backPress", () => {
      if (this.state.appState == AppState.home) {
        if (!this.quit) {
          ToastAndroid.showWithGravity(
            "Pressione voltar novamente para sair.",
            ToastAndroid.SHORT,
            ToastAndroid.CENTER
          );
          this.quit = true;
          setTimeout(() => {
            this.quit = false;
          }, 2000);
        } else {
          BackHandler.exitApp();
        }
      } else {
        this.goBack();
      }
      return true;
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("backPress");
  }
}

function mountRegistries(registries, zoom) {
  let list = [];
  registries.forEach(props => {
    list.push(<TrunkPlant {...props} zoom={zoom} />);
  });
  return list;
}

function calcZoom(longitudeDelta) {
  return Math.log(360 / longitudeDelta) / Math.LN2 - 1;
}
