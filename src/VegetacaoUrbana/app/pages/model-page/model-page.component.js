import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { headerHeight, colors } from "../../app-styles";
import PropTypes from "prop-types";

export default class Page extends Component {
  render() {
    console.log(headerHeight);
    return (
      <View
        {...this.props}
        style={[
          styles.container,
          {
            backgroundColor: this.props.backgroundColor
          }
        ]}
      >
        {this.props.children}
      </View>
    );
  }
}

Page.propTypes = {
  backgroundColor: PropTypes.string
};

Page.defaultProps = {
  backgroundColor: colors.white
};

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: headerHeight,
    right: 0,
    left: 0,
    bottom: 0
  }
});
