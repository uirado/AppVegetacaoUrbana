import { StyleSheet } from "react-native"
import { colors } from "../../app-styles"

const styleSignupPage = StyleSheet.create({
    styleSignupView: {
        flex: 1,
        flexDirection: "column",
        alignContent: "center",
    },
    styleIcon: {
        padding: "10%",
        alignItems: "center"
    },
    styleForm: {
        padding: "20%"
    },
    styleTexts: {
        color: colors.primary
    }
})

export default styleSignupPage;