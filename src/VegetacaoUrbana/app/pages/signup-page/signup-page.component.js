import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styleSignupPage from "./signup-page.styles";
import { colors } from "../../app-styles";
import Header from "../../components/header/header.component";
import ConfirmationOptions from "../../components/confirmation-options/confirmation-options";

export default class SignupPage extends Component {
  render() {
    return (
      <View style={styleSignupPage.styleSignupView}>
        <View>
          <Header leftIcon={(Icon.name = "tree")} title={"Cadastrar"} />
        </View>
        <View style={styleSignupPage.styleForm}>
          <TextInput placeholder="Nome" />
          <TextInput placeholder="Email" />
          <TextInput placeholder="Senha" secureTextEntry={true} />
          <TextInput placeholder="Confirmar Senha" secureTextEntry={true} />
        </View>
        <ConfirmationOptions
          cancelAction={() => {
            this.props.navigation.goBack();
          }}
          confirmAction={() => {
            this.props.navigation.navigate("Home");
          }}
        />
      </View>
    );
  }
}
