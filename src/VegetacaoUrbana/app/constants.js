export const BrazilRegion = {
  latitude: -14.516087127319578,
  longitude: -53.74179610982537,
  latitudeDelta: 62.06396556910977,
  longitudeDelta: 42.187478877604015
};

export const form = {
  inputHeight: 40
};

export const DefaultDeltas = {
  normal: {
    latitudeDelta: 0.00819121723770877,
    longitudeDelta: 0.005839504301548004
  },
  segment: {
    latitudeDelta: 0.0021016677584100307,
    longitudeDelta: 0.0014983490109443665
  },
  registry: {
    longitudeDelta: 0.0008579716086387634,
    latitudeDelta: 0.001352755982507503
  }
};
