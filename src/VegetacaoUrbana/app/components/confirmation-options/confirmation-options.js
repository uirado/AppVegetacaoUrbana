import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Text } from "react-native";
import HideWithKeyboard from "../hide-with-keyboard.component";
import PropTypes from "prop-types";
import { colors } from "../../app-styles";

export default class ConfirmationOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { cancelTitle, confirmTitle } = this.props;
    return (
      <HideWithKeyboard style={styles.container}>
        <View style={[styles.buttonBox, { backgroundColor: colors.danger }]}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={[styles.button]}
            onPress={this.props.cancelAction}
          >
            <View style={styles.buttonTitleContainer}>
              <Text
                style={[
                  styles.buttonTitle,
                  { color: colors.text, fontWeight: "normal" }
                ]}
              >
                {cancelTitle.toUpperCase()}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.spacer} />
        <View style={[styles.buttonBox, { backgroundColor: colors.secondary }]}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.button}
            onPress={this.props.confirmAction}
          >
            <View style={styles.buttonTitleContainer}>
              <Text style={[styles.buttonTitle, { color: colors.secondary }]}>
                {confirmTitle.toUpperCase()}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </HideWithKeyboard>
    );
  }
}

ConfirmationOptions.propTypes = {
  confirmTitle: PropTypes.string,
  cancelTitle: PropTypes.string
};
ConfirmationOptions.defaultProps = {
  confirmTitle: "Confirmar",
  cancelTitle: "Cancelar"
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    position: "absolute",

    width: "100%",
    bottom: 0,
    backgroundColor: colors.white,
    borderTopColor: colors.gray,
    borderTopWidth: 0.5
  },
  buttonBox: {
    flex: 1
  },
  button: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    height: 50,
    backgroundColor: colors.white
  },
  buttonTitleContainer: {
    width: "100%",
    alignItems: "center"
  },
  buttonTitle: {
    fontSize: 14,
    fontWeight: "bold",
    color: colors.white
  },
  spacer: {
    backgroundColor: colors.gray,
    width: 1,
    height: 50
  }
});
