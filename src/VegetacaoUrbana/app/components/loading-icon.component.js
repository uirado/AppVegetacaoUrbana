import React, { Component } from "react";
import { View, ActivityIndicator, Text } from "react-native";

export default class LoadingIcon extends Component {
  render() {
    if (this.props.simple) {
      return (
        <View
          style={{
            flexDirection: "row",
            alignContent: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator style={{ marginHorizontal: 5 }} size="small" />
          {this.props.message && <Text>{this.props.message}</Text>}
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            height: "100%",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#fff8"
          }}
        >
          <View
            style={{
              padding: 20,
              borderRadius: 20,
              backgroundColor: "#fff",
              elevation: 2
            }}
          >
            <ActivityIndicator size="large" />
            {this.props.message && <Text>{this.props.message}</Text>}
          </View>
        </View>
      );
    }
  }
}
