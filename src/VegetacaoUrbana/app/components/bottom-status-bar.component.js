import React, { Component } from "react";
import { View, Text, ActivityIndicator } from "react-native";
import PropTypes from "prop-types";
import { colors } from "../app-styles";

export default class BottomStatusBar extends Component {
  render() {
    return (
      <View
        style={{
          position: "absolute",
          padding: 3,
          left: 0,
          bottom: 0,
          right: 0,
          backgroundColor: this.props.bg,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {this.props.ActivityIndicator && (
          <ActivityIndicator size={16} color={colors.secondary} />
        )}
        <Text
          style={{
            color: colors.secondary,
            fontSize: 10,
            fontWeight: "bold",
            marginHorizontal: 10
          }}
        >
          {String(this.props.message).toUpperCase()}
        </Text>
      </View>
    );
  }
}

BottomStatusBar.propTypes = {
  bg: PropTypes.string
};

BottomStatusBar.defaultProps = {
  bg: colors.white + "80"
};
