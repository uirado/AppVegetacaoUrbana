import React, { Component } from "react";
import Plant from "./plant.component";
import PropTypes from "prop-types";
import { colors } from "../../app-styles";

//enum
const TrunkPlantType = {
  small: "SMALL",
  medium: "MEDIUM",
  large: "LARGE",
  palm: "PALM"
};

export default class TrunkPlant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      iconDiameter: cupToIconDiameter(props.type, props.cupDiameter)
    };
  }

  render() {
    return (
      <Plant
        {...this.props}
        diameter={this.state.iconDiameter}
        icon={resolveIcon(this.props.type)}
        color={resolveColor(this.props.type)}
      />
    );
  }
}

function resolveColor(type) {
  switch (type) {
    case TrunkPlantType.small:
      return colors.primary;
    default:
      return colors.darkGreen;
  }
}

function resolveIcon(type) {
  switch (type) {
    case TrunkPlantType.palm:
      return "asterisk";
    default:
      return "circle";
  }
}

function cupToIconDiameter(type, diameter) {
  if (diameter && diameter >= 5) {
    return diameter;
  } else {
    switch (type) {
      case TrunkPlantType.small:
        return 10;
      case TrunkPlantType.medium:
        return 15;
      case TrunkPlantType.large:
        return 20;
      case TrunkPlantType.palm:
        return 10;
      default:
        return 10;
    }
  }
}

//verifica os tipos passados pelos props e sua obrigatoriedade
TrunkPlant.propTypes = {
  type: PropTypes.string.isRequired,
  trunkDiameter: PropTypes.number,
  cupDiameter: PropTypes.number
};

export { TrunkPlantType };
