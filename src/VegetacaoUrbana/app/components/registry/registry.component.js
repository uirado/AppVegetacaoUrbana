import React, { Component } from "react";
import { Marker, LatLng, Callout } from "react-native-maps";
import PropTypes from "prop-types";

import Icon from "./marker-icon.component";
import Balloon from "./info-balloon/info-balloon.component";
import { AppState } from "../../state-manager";

export default class Registry extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Marker
        ref={ref => (this.marker = ref)}
        key={Date.now()} //hack pra atualizar marker
        anchor={
          (this.props.type == "UNDERBRUSH" && {
            x: 0.5,
            y: 1
          }) || {
            x: 0.5,
            y: 0.5
          }
        }
        tracksViewChanges={false}
        coordinate={{
          latitude: this.props.latitude,
          longitude: this.props.longitude
        }}
        title="Árvore de grande porte"
      >
        <Icon
          zoom={this.props.zoom}
          diameter={this.props.diameter}
          icon={this.props.icon}
          color={this.props.color}
        />
        <Callout
          tooltip={false}
          onPress={() => {
            this.props.gotoState(AppState.editRegistry, {
              registryId: this.props.id
            });
          }}
        >
          <Balloon type={this.props.type} id={this.props.id} />
        </Callout>
      </Marker>
    );
  }
}

Registry.propTypes = {
  showBalloon: PropTypes.bool,
  diameter: PropTypes.number.isRequired,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  annotation: PropTypes.shape([String]),
  creationDate: PropTypes.shape(Date),
  updateDate: PropTypes.shape(Date),
  pictures: PropTypes.shape([String])
};

Registry.defaultProps = {
  showBalloon: true
};
