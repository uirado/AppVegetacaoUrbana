import React, { Component } from "react";
import { View, PixelRatio } from "react-native";
import { colors } from "../../app-styles";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class MarkerIcon extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scale: Math.pow(2, this.props.zoom - 1) / 27000
    };
  }

  render() {
    let size = Math.round(this.state.scale * this.props.diameter);
    let color = colors.darkGreen;
    if (this.props.color) {
      color = this.props.color;
    }
    return (
      <Icon
        style={{
          opacity: 0.8
        }}
        name={this.props.icon}
        color={color}
        size={size}
      />

      // <View
      //   style={[
      //     {
      //       borderRadius: radius,
      //       backgroundColor: colors.plantGreen,
      //       opacity: 0.8,
      //       width: width,
      //       height: height
      //     }
      //   ]}
      // />
    );
  }
}

MarkerIcon.propTypes = {
  diameter: PropTypes.number.isRequired,
  zoom: PropTypes.number.isRequired,
  icon: PropTypes.string.isRequired
};
