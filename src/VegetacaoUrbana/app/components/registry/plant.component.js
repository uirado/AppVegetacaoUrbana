import React, { Component } from "react";
import Registry from "./registry.component";
import PropTypes from "prop-types";

export default class Plant extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <Registry {...this.props} />;
  }
}

Plant.propTypes = {
  height: PropTypes.number,
  lotDistance: PropTypes.number,
  streetDistance: PropTypes.number
};
