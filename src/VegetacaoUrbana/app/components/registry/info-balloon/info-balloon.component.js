import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import LoadingIcon from "../../loading-icon.component";
import styles from "./info-balloon.styles";

const GET_IMAGE_BY_ID = gql`
  query Registry($id: ID!) {
    Registry(id: $id) {
      images
    }
  }
`;

export default class InfoBalloon extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.image}>
          {this.props.id && (
            <Query query={GET_IMAGE_BY_ID} variables={{ id: this.props.id }}>
              {({ loading, error, data }) => {
                if (loading) return <LoadingIcon simple />;
                if (error) return <Text>Sem Foto</Text>;

                if (data && data.Registry && data.Registry.images) {
                  return (
                    <Image
                      style={{
                        width: "100%",
                        height: "100%",
                        resizeMode: Image.resizeMode.cover
                      }}
                      source={{
                        uri: data.Registry.images[0]
                      }}
                    />
                  );
                } else {
                  return <Text>Sem Foto</Text>;
                }
                return null;
              }}
            </Query>
          )}
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.text}>Ver detalhes</Text>
        </View>
      </View>
    );
  }
}
