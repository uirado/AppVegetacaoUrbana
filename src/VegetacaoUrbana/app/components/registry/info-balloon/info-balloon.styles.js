import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../../app-styles";
const { width, height } = Dimensions.get("screen");

export default StyleSheet.create({
  container: {},
  textContainer: {
    height: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 12,
    fontWeight: "normal",
    color: colors.secondary
  },
  image: {
    height: width / 3,
    width: width / 3,
    padding: 3,
    backgroundColor: colors.lightGray,
    alignItems: "center",
    justifyContent: "center"
  }
});
