import React, { Component } from "react";
import Plant from "./plant.component";
import PropTypes from "prop-types";

//enum
const TrunkPlantType = {
  small: "SMALL",
  medium: "MEDIUM",
  large: "LARGE",
  palm: "PALM"
};

export default class Underbrush extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <Plant {...this.props} icon="barley" diameter={8} />;
  }
}

//verifica os tipos passados pelos props e sua obrigatoriedade
Underbrush.propTypes = {
  type: PropTypes.string.isRequired
};

export { TrunkPlantType };
