import React, { Component } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import { LatLng, Polyline } from "react-native-maps";

export default class Segment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coordinates: this.props.coordinates,
      color: "#aaaaaa"
    };
  }

  render() {
    return (
      <View>
        <Polyline
          coordinates={this.state.coordinates}
          strokeWidth={5}
          geodesic
          strokeColor={this.state.color + "80"}
        />
        {this.props.children}
      </View>
    );
  }

  addCoordinate(coordinate) {
    //this.coordinates.push(coordinate);
  }
}

Segment.propTypes = {
  coordinates: PropTypes.arrayOf(PropTypes.shape(LatLng)).isRequired
};
