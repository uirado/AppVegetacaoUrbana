import React, { Component } from "react";
import { TouchableOpacity, Text, View } from "react-native";
import styles from "./generic-button.styles";
import { colors } from "../../app-styles";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class GenericButton extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    if (this.props.visible) {
      return (
        <View style={[styles.container, styles.box]}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.box,
              styles.button,
              { backgroundColor: this.props.bgColor }
            ]}
            onPress={this.props.onPress}
          >
            {this.props.icon && (
              <Icon
                name={this.props.icon}
                size={30}
                color={this.props.titleColor}
                style={styles.icon}
              />
            )}
            <View style={styles.buttonTitleContainer}>
              <Text
                style={[styles.buttonTitle, { color: this.props.titleColor }]}
                numberOfLines={1}
              >
                {this.props.title}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }
}

GenericButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string,
  bgColor: PropTypes.string,
  titleColor: PropTypes.string,
  visible: PropTypes.bool
};

GenericButton.defaultProps = {
  bgColor: colors.secondary,
  titleColor: colors.white,
  visible: true
};
