import { StyleSheet } from "react-native";
import { colors } from "../../app-styles";

const SIZE = 60;
const RADIUS = SIZE / 2;
export default StyleSheet.create({
  box: {
    alignItems: "center",
    borderRadius: RADIUS
  },
  container: {
    backgroundColor: colors.white,
    position: "absolute",
    bottom: 30,
    right: 30,
    elevation: 3,

    borderRadius: RADIUS,
    height: SIZE,
    width: SIZE
  },
  icon: {
  },
  button: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    borderRadius: RADIUS,
    height: SIZE,
    width: SIZE
  },
  buttonTitleContainer: {
    position: "absolute",
    width: "100%",
    alignItems: "center"
  },
  buttonTitle: {
    fontSize: 16,
    fontWeight: "bold"
  }
});
