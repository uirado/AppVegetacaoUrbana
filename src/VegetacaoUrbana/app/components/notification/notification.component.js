import React, { Component } from "react";
import { View, Text, TouchableHighlight } from "react-native";
import PropTypes from "prop-types";
import { colors } from "../../app-styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import styles from "./notification.styles.js";

export default class Notification extends Component {
  constructor(props) {
    super(props);

    const { icon, color, iconColor } = resolveType(this.props.type);

    this.state = {
      show: this.props.show,
      icon: icon,
      color: color,
      iconColor: iconColor
    };
  }

  componentWillReceiveProps(props) {
    const { icon, color, iconColor } = resolveType(this.props.type);

    this.setState({
      show: props.show,
      icon: icon,
      color: color,
      iconColor: iconColor
    });
  }

  render() {
    return (
      this.state.show && (
        <View style={styles.container}>
          <View
            style={[styles.box, { backgroundColor: this.state.color + "33" }]}
          >
            {this.state.icon && (
              <Icon
                name={this.state.icon}
                size={30}
                color={this.state.iconColor}
                style={styles.icon}
              />
            )}
            <Text style={styles.text}>{this.props.text}</Text>
            <TouchableHighlight
              underlayColor={colors.underlayColor}
              style={styles.closeButton}
              onPress={this.props.closeAction}
            >
              <Icon name="close" color={colors.text} size={20} />
            </TouchableHighlight>
          </View>
        </View>
      )
    );
  }
}

function resolveType(type) {
  var result = {
    icon: "alert-circle-outline",
    color: colors.white,
    iconColor: colors.text
  };

  switch (type) {
    case "information":
      result.icon = "information-outline";
      result.color = colors.secondary;
      break;
    case "danger":
      result.icon = "alert-circle";
      result.color = colors.danger;
      break;
    case "alert":
      result.icon = "alert-outline";
      result.color = colors.alert;
      break;
    case "success":
      result.icon = "check-circle-outline";
      result.color = colors.success;
      break;
  }

  result.iconColor = result.color;

  if (result.color == colors.white) {
    result.iconColor = colors.text;
  }

  return result;
}

Notification.propTypes = {
  show: PropTypes.bool,
  type: PropTypes.string,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string,
  closeAction: PropTypes.func.isRequired
};

Notification.defaultProps = {
  type: null,
  icon: "alert-circle-outline",
  color: colors.white,
  iconColor: colors.text,
  show: true
};
