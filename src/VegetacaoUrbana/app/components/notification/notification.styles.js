import { StyleSheet } from "react-native";
import { colors } from "../../app-styles";
import { headerHeight } from "../../app-styles";
import { Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    position: "absolute",
    alignSelf: "flex-end",
    top: headerHeight,
    margin: 10,
    right: 0,
    borderRadius: 3,
    backgroundColor: colors.white,
    elevation: 2
  },
  box: {
    borderRadius: 3,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: colors.secondary + "33",
    paddingRight: 40,
    paddingLeft: 40,
    paddingTop: 3,
    paddingBottom: 5,
    minHeight: 40
  },
  text: {
    color: colors.text,
    fontSize: 14,
    fontWeight: "normal"
  },
  icon: {
    position: "absolute",
    left: 5
  },
  closeButton: {
    position: "absolute",
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    width: 30,
    height: 30,
    right: 5
  }
});
