import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { colors } from "../app-styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Pino extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.head}>
          <View style={styles.circle} />
        </View>
        <View style={styles.stick} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    alignItems: "center",
    marginBottom: 30
  },
  circle: {
    width: 5,
    height: 5,
    borderRadius: 2.5,
    backgroundColor: colors.primary
  },
  head: {
    alignItems: "center",
    justifyContent: "center",
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: colors.black
  },
  stick: {
    backgroundColor: colors.black,
    width: 2,
    height: 10
  }
});
