import { StyleSheet } from "react-native";
import { colors } from "../../app-styles";
import { headerHeight } from "../../app-styles";

export default StyleSheet.create({
  mainContainer: {
    position: "absolute",
    top: 0,
    width: "100%",
    alignItems: "center"
  },
  headerContainer: {
    elevation: 3,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    width: "100%",
    height: headerHeight,
    backgroundColor: colors.primary
  },
  IconContainer: {
    width: headerHeight,
    height: headerHeight,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: headerHeight / 2
  },
  leftIcon: {
    color: colors.darkGreen,
    fontSize: 30
  },
  rightIcon: {
    color: colors.white,
    fontSize: 30
  },
  titleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    padding: 0,
    margin: 0,
    color: colors.darkGreen,
    fontSize: 20,
    fontWeight: "bold"
  },
  subTitle: {
    color: colors.darkGreen,
    fontSize: 14,
    fontWeight: "normal",
    lineHeight: 16
  }
});
