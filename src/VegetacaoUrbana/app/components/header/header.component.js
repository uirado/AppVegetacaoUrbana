import React, { Component } from "react";
import { View, Text, TouchableHighlight } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./header.styles.js";
import { colors } from "../../app-styles";

export default class Header extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <View style={styles.IconContainer}>
            {this.props.leftIcon && (
              <TouchableHighlight
                style={styles.IconContainer}
                underlayColor={colors.underlayColor}
                onPress={this.props.leftAction}
              >
                <Icon name={this.props.leftIcon} style={styles.leftIcon} />
              </TouchableHighlight>
            )}
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.title} numberOfLines={1}>
              {this.props.title}
            </Text>
            {this.props.subTitle && (
              <Text style={styles.subTitle} numberOfLines={1}>
                {this.props.subTitle}
              </Text>
            )}
          </View>
          <View style={styles.IconContainer}>
            {this.props.rightIcon && (
              <TouchableHighlight
                style={styles.IconContainer}
                underlayColor={colors.underlayColor}
                onPress={this.props.rightAction}
              >
                <Icon name={this.props.rightIcon} style={styles.rightIcon} />
              </TouchableHighlight>
            )}
          </View>
        </View>
      </View>
    );
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  rightAction: PropTypes.func,
  rightAction: PropTypes.func
};
