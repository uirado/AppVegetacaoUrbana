import React, { Component } from "react";
import { PermissionsAndroid } from "react-native";
import MapView from "react-native-maps";
import styles, { customStyle } from "./map.styles";

export default class Mapa extends Component {
  constructor(props) {
    super(props);

    this.state = {
      region: this.props.region
    };
  }

  render() {
    return (
      <MapView
        initialRegion={this.props.region}
        style={styles.map}
        ref={map => {
          this.map = map;
        }}
        showsUserLocation={true}
        mapType="standard"
        showsMyLocationButton={true}
        loadingEnabled={true}
        customMapStyle={customStyle}
        onRegionChangeComplete={this._onRegionChangeComplete}
        moveOnMarkerPress={this.props.moveOnMarkerPress}
      >
        {this.props.children}
      </MapView>
    );
  }

  _onRegionChangeComplete = newRegion => {
    console.log(newRegion);
    this.props.onRender(newRegion);
  };

  async requestLocation() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Vegetação Urbana App Location Permission",
          message:
            "Vegetação Urbana App needs access to your location " +
            "so you can have a great experience."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location");
        return true;
      } else {
        console.log("Location permission denied");
        return false;
      }
    } catch (err) {
      console.warn(err);
      return false;
    }
  }

  gotoRegion(region) {
    this.map.animateToRegion(region);
  }

  zoomToUser() {
    if (!this.alreadyZoomed && this.requestLocation()) {
      this.alreadyZoomed = true;
      navigator.geolocation.getCurrentPosition(
        ({ coords }) => {
          if (this.map) {
            newRegion = {
              latitude: coords.latitude,
              longitude: coords.longitude,
              latitudeDelta: 0.005,
              longitudeDelta: 0.005
            };
            this.gotoRegion(newRegion);
          }
        },
        error => {
          console.log("Error: Are location services on?");
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
    }
  }
}

//implementar back a partir do navigator
