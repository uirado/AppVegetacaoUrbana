import { DefaultDeltas, BrazilRegion } from "./constants";

export const AppState = {
  home: 1,
  addRegistry: 2,
  editRegistry: 3,
  viewRegistry: 4,
  login: 5,
  signUp: 6
};

export const StateTexts = {
  home: {
    title: "Vegetação Urbana",
    subTitle: null,
    notification: "Adicione uma nova vegetação ou toque em uma já existente."
  },
  editRegistry: {
    title: "Detalhes da vegetação",
    subTitle: null,
    notification: "Faça as alterações necessárias, depois clique em salvar."
  },
  addRegistry: {
    title: "Adicionar vegetação",
    notification: "Mova o pino para o local desejado e pressione Confirmar"
  }
};

export function getInitialState() {
  return {
    zoom: 1,
    region: BrazilRegion,
    appState: AppState.home,
    headerTitle: StateTexts.home.title,
    headerSubTitle: StateTexts.home.subTitle,
    headerLeftIcon: "tree",
    headerRightIcon: "information-outline",
    notificationText: StateTexts.home.notification,
    showNotification: true,
    moveOnMarkerPress: true,
    shouldRender: true,
    registryId: null
  };
}

export function getState(nextState, currentState) {
  newState = {
    region: Object.assign({}, currentState.region),
    appState: nextState
  };
  switch (nextState) {
    case AppState.home:
      newState.headerTitle = StateTexts.home.title;
      newState.headerSubTitle = StateTexts.home.subTitle;
      newState.headerLeftIcon = "tree";
      newState.headerRightIcon = "information-outline";
      newState.notificationText = StateTexts.home.notification;
      newState.showNotification = true;
      newState.moveOnMarkerPress = true;
      newState.shouldRender = true;
      newState.registryId = null;
      break;

    case AppState.addRegistry:
      newState.region.latitudeDelta = DefaultDeltas.registry.latitudeDelta;
      newState.region.longitudeDelta = DefaultDeltas.registry.longitudeDelta;
      newState.headerTitle = StateTexts.addRegistry.title;
      newState.headerSubTitle = StateTexts.addRegistry.subTitle;
      newState.headerLeftIcon = "tree";
      newState.headerRightIcon = "information-outline";
      newState.notificationText = StateTexts.addRegistry.notification;
      newState.showNotification = true;
      newState.shouldRender = true;
      break;

    case AppState.editRegistry:
      newState.headerTitle = StateTexts.editRegistry.title;
      newState.headerSubTitle = StateTexts.editRegistry.subTitle;
      newState.headerLeftIcon = "tree";
      newState.headerRightIcon = null;
      newState.headerLeftAction = newState.notificationText =
        StateTexts.editRegistry.notification;
      newState.showNotification = false;
      newState.shouldRender = true;
      break;

    default:
      return null;
      break;
  }
  return newState;
}
