import React, { Component } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import TrunkPlant from "../components/registry/trunk-plant.component";
import Underbrush from "../components/registry/underbrush.component";
import { Text, View } from "react-native";
import { Marker } from "react-native-maps";
import BottomStatusBar from "../components/bottom-status-bar.component";
import { AppState } from "../state-manager";

const allRegistriesQuery = gql`
  query {
    allRegistries {
      id
      latitude
      longitude
      type
      cupDiameter
    }
  }
`;

export default ({ zoom: zoom, gotoState: gotoState }) => (
  <Query query={allRegistriesQuery} pollInterval={5000}>
    {({ loading, error, data }) => {
      if (loading) {
        return null;
      }
      if (error) return null;

      return data.allRegistries.map(
        registry =>
          (registry.type == "UNDERBRUSH" && (
            <Underbrush
              {...registry}
              zoom={zoom}
              key={registry.id}
              gotoState={gotoState}
            />
          )) || (
            <TrunkPlant
              {...registry}
              zoom={zoom}
              key={registry.id}
              gotoState={gotoState}
            />
          )
      );
    }}
  </Query>
);
