import { StyleSheet } from "react-native";

const colors = {
  primary: "#00e676",
  secondary: "#4286f4",
  text: "#333333",
  darkGreen: "#0b6623",
  white: "#FFFFFF",
  black: "#111111",

  lightGray: "#eeeeee",
  gray: "#CCCCCC",
  darkGray: "#666666",

  danger: "#FF0000",
  alert: "#ffbb00",
  success: "#00e676",

  underlayColor: "#000000" + "19"
};

export const headerHeight = 54;

export { colors };
