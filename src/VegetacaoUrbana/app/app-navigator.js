import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import { StackNavigator } from "react-navigation";
import HomePage from "./pages/home-page/home-page.component";
import EditRegistry from "./pages/edit-registry-page/edit-registry-page.component";
import styles from "./app-styles";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import gql from "graphql-tag";
import LoginPage from "./pages/login-page/login-page.component";
import SignupPage from "./pages/signup-page/signup-page.component";

const client = new ApolloClient({
  uri: `https://api.graph.cool/simple/v1/cjgzoroyz04qe01463r8s7979`
});

const RootStack = StackNavigator(
  {
    Login: {
      screen: LoginPage
    },
    Home: {
      screen: HomePage
    },
    Signup: {
      screen: SignupPage
    }
  },
  {
    initialRouteName: "Login",
    headerMode: "none",
    // navigationOptions: {
    //   title: "Home",
    //   headerStyle: styles.header,
    //   headerTitleStyle: styles.headerTitle
    // },
    cardStyle: { backgroundColor: "transparent" }
  }
);

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <View style={{ flex: 1 }}>
          <RootStack pointerEvents="box-none" />
        </View>
      </ApolloProvider>
    );
  }
}
